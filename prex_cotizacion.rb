require 'nokogiri'
require 'open-uri'
require 'telegram/bot'
require 'byebug'

# Obtener la cotización del dólar en PrexCard
def obtener_cotizacion_prex
  url = 'https://www.prexcard.com.ar/transferencias-uruguay'
  doc = Nokogiri::HTML(URI.open(url))
  cotizacion = doc.css('.valor-cotizacion').text.strip
  cotizacion.split("\n").map(&:to_f)
end

RETRY_DELAY = 60

def execute_with_retry
  yield
rescue StandardError => e
  puts e.message
  sleep(60)
  retry
end

while true
  prex_venta ||= 0
  prex_compra ||= 0
  blue_venta ||= 0
  blue_compra ||= 0
  execute_with_retry do
    # Enviar mensaje a Telegram
    def send_telegram(mensaje)
      Telegram::Bot::Client.new(ENV['TG_TOKEN']).send_message(chat_id: ENV['CHAT_ID'], text: mensaje)
    end

    # Obtener la cotización y enviar mensaje a Telegram
    prex_venta_now, prex_compra_now = obtener_cotizacion_prex
    if prex_compra_now > prex_compra && prex_compra_now > 0
      prex_venta = prex_venta_now
      prex_compra = prex_compra_now
      mensaje = "La cotización actual del dólar en PrexCard SUBIÓ:\nPrecio compra: $#{prex_compra} ARS"
    elsif prex_compra_now < prex_compra && prex_compra_now > 0
      prex_venta = prex_venta_now
      prex_compra = prex_compra_now
      mensaje = "La cotización actual del dólar en PrexCard BAJÓ:\nPrecio compra: $#{prex_compra} ARS"
    end
    send_telegram(mensaje)
    sleep 60
  end
end
