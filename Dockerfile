FROM ruby:3.0.0
WORKDIR /prex
COPY . .
RUN bundle install
CMD ruby prex_cotizacion.rb
